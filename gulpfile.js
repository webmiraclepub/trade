'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    rigger = require('gulp-rigger'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    plumber = require('gulp-plumber'),
    newer = require('gulp-newer'),
    livereload = require('gulp-livereload'),
    htmlmin = require('gulp-htmlmin'),
    browserSync = require("browser-sync"),
    babel = require('gulp-babel');
//reload = browserSync.reload;

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.html',
        js: ([
            'node_modules/jquery/dist/jquery.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            /*'node_modules/slick-carousel/slick/slick.js',*/
            'node_modules/jquery-slimscroll/jquery.slimscroll.js',
            'node_modules/jquery.cookie/jquery.cookie.js',
            //'node_modules/jquery-ui-slider/jquery-ui.js',
            //'node_modules/jquery-ui-touch-punch/jquery.ui.touch-punch.js',
            'src/js/jquery-ui.min.js',
            'src/js/parallax.min.js',
            'src/js/calculator.js',
            'src/js/bounty.js',
            'src/js/scripts.js'
        ]),
        style: [
            'src/scss/main.scss'
        ],
        styleMedia: [

        ],
        img: [
            'src/img/**/*.*'
        ],
        fonts: [
            'src/fonts/**/*.*'
        ]
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/scss/**/*.scss',
        styleMedia: 'src/scss/medias/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    },
    tunnel: true,
    host: 'localhost',
    port: 7000,
    logPrefix: "Frontend"
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(path.build.html))
        .pipe(livereload());
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(newer(path.build.js))
        .pipe(plumber())
        .pipe(babel({
            presets: ['es2015']
        }))
        // .pipe(rigger())
        .pipe(concat('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(livereload());
});

gulp.task('style:build', function () {
    gulp.src(path.src.style)
        // .pipe(newer(path.build.css))
        .pipe(plumber())
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
        .pipe(livereload());
});

gulp.task('styleMedia:build', function () {
    gulp.src(path.src.styleMedia)
        .pipe(newer(path.build.css))
        .pipe(plumber())
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
        .pipe(livereload());
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('image:build', function () {
    gulp.src(path.src.img) 
        // .pipe(imagemin({
        //     progressive: true,
        //     svgoPlugins: [{removeViewBox: false}],
        //     use: [pngquant()],
        //     interlaced: true
        // }))
        .pipe(gulp.dest(path.build.img));
        //.pipe(reload({stream: true}));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'styleMedia:build',
    'image:build',
    'fonts:build'
]);

gulp.task('watch', function(){
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('styleMedia:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('default', ['build', 'webserver', 'watch']);
//gulp.task('default', ['build', 'watch']);