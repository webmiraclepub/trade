$(document).ready(function() {
    // ========================================================================= SHOW MAIN CALCULATOR BLOCK

    $(".show-plan").click(function (e) {


            $(".main-calculator").slideToggle("slow");

    });

    // ========================================================================= SHOW SELECT-BOX
    $(function () {
        $('body').on('click', '.select-plan .select-link', function () {
            $(".main-calculator .select-plan").removeClass('active');
            var navMenu = $(this).parent('.select-plan');

            if ($(navMenu).hasClass('active')) {
                $(navMenu).removeClass('active');
            }
            else {
                $(navMenu).addClass('active');
            }
            return false;
        });

    });
    $('.select-plan .select-list li a').on('click', function () {
        $(this).closest('.select-plan').find('.select-link > a').html($(this).html());
    });
    $(document).click(function (event) {
        if (!$(event.target).hasClass('select-tab')) {
            $('.main-calculator .select-plan').removeClass('active');
        }
    });

    // ========================================================================= SELECT-BOX PLANS OPERATIONS
    $('#calculator-direction .select-list li a').on('click', function(){
        var direction = $(this).data('direction');
        $("#calculator-plans .plan-direction").removeClass('active');
        $("#calculator-plans .plan-direction."+direction).addClass('active');

        $("#calculator-plans .plan-direction a").removeClass('selected');
        $("#calculator-plans .plan-direction.active a").first().addClass('selected');
        var firstDirectionPlan = $("#calculator-plans .plan-direction.active a").first().html();
        $("#calculator-plans .select-link > a").html(firstDirectionPlan);

        setTimeout(function () {
            var selectedPlan = $('#calculator-plans .select-list li a.selected').data('minimum');
            var currencyRate = parseInt($("#calculator-currency .select-list li a.selected").data('rate'));
            var planMinimumAmount = selectedPlan/currencyRate;
            $('#calculator-amount').val(planMinimumAmount);

            mainCalculator();
        }, 50);
    });
    $('#calculator-plans .select-list li a').on('click', function() {
        $("#calculator-plans .select-list li a").removeClass('selected');
        $(this).addClass('selected');

        var selectedPlan = $(this).data('minimum');
        var currencyRate = parseInt($("#calculator-currency .select-list li a.selected").data('rate'));
        var planMinimumAmount = selectedPlan/currencyRate;
        $('#calculator-amount').val(planMinimumAmount);

        setTimeout(function () {
            mainCalculator();
        }, 50);
    });
    $('#calculator-currency .select-list li a').on('click', function() {
        $("#calculator-currency .select-list li a").removeClass('selected');
        $(this).addClass('selected');

        var selectedPlan = $('#calculator-plans .select-list li a.selected').data('minimum');
        var currencyRate = parseInt($(this).data('rate'));
        var planMinimumAmount = selectedPlan/currencyRate;
        $('#calculator-amount').val(planMinimumAmount);

        setTimeout(function () {
            mainCalculator();
        }, 50);
    });

    // ========================================================================= MAIN CALCULATOR FUNCTION
    function mainCalculator() {
        var to_fixed = 6;
        var currencyPayment = parseInt($('#calculator-currency .select-list li a.selected').data('payment'));
        var currencyRate = parseInt($('#calculator-currency .select-list li a.selected').data('rate'));
        if(currencyPayment==18){
            to_fixed=2;
        }

        var planPercent = $('#calculator-plans .select-list li a.selected').data('value'),
            planDays = $('#calculator-plans .select-list li a.selected').data('days'),
            plan = $('#calculator-plans .select-list li a.selected').data('plan');

        if(!$('#calculator-plans .select-list li a.selected').length){
            $("#calculator-plans .select-list li:first-of-type a").addClass("selected");
            $("#calculator-plans .select-link > a").html($("#calculator-plans .select-list li:first-of-type a").html());
        }
        var amount = $('#calculator-amount').val();
        if(amount == ''){
            var selectedPlan = $('#calculator-plans .select-list li a.selected').data('minimum');
            var currencyRate = parseInt($('#calculator-currency .select-list li a.selected').data('rate'));
            var planMinimum = selectedPlan/currencyRate;
            $('#calculator-amount').val(planMinimum);
        }

        if(plan <= 6) {
            var totalProfit = (parseFloat(planPercent) / 100 * parseFloat(amount)).toFixed(to_fixed);
            var dailyProfit = "<small>on complete</small>";
        }
        else if(plan == 7){
            var difference_amount = amount*currencyRate;
            if(difference_amount >=  50001){
                var totalProfit = (5 / 100 * parseFloat(amount) * planDays).toFixed(to_fixed);
                var dailyProfit = (5 / 100 * parseFloat(amount) * 1).toFixed(to_fixed);
            }
            else if(difference_amount >=  30001 && difference_amount <= 50000){
                var totalProfit = (4.8 / 100 * parseFloat(amount) * planDays).toFixed(to_fixed);
                var dailyProfit = (4.8 / 100 * parseFloat(amount) * 1).toFixed(to_fixed);
            }
            else if(difference_amount >=  10001 && difference_amount <= 30000){
                var totalProfit = (4.6 / 100 * parseFloat(amount) * planDays).toFixed(to_fixed);
                var dailyProfit = (4.6 / 100 * parseFloat(amount) * 1).toFixed(to_fixed);
            }
            else if(difference_amount >=  1001 && difference_amount <= 10000){
                var totalProfit = (4.4 / 100 * parseFloat(amount) * planDays).toFixed(to_fixed);
                var dailyProfit = (4.4 / 100 * parseFloat(amount) * 1).toFixed(to_fixed);
            }
            else if(difference_amount >=  101 && difference_amount <= 1000){
                var totalProfit = (4.2 / 100 * parseFloat(amount) * planDays).toFixed(to_fixed);
                var dailyProfit = (4.2 / 100 * parseFloat(amount) * 1).toFixed(to_fixed);
            }
            else if(difference_amount >=  1 && difference_amount <= 100){
                var totalProfit = (4 / 100 * parseFloat(amount) * planDays).toFixed(to_fixed);
                var dailyProfit = (4 / 100 * parseFloat(amount) * 1).toFixed(to_fixed);
            }
        }
        else if(plan >= 8){
            var totalProfit = (parseFloat(planPercent) / 100 * parseFloat(amount)).toFixed(to_fixed);
            var dailyProfit = "<small>on complete</small>";
        }

        if (!$('#calculator-amount').val()){
            $('#calculator-daily').text('');
            $('#calculator-total').text('');
        }
        else{
            $('#calculator-daily').html(dailyProfit);
            if (totalProfit) {
                $('#calculator-total').text(totalProfit);
            }
        }
    }

    // ========================================================================= MAIN CALCULATOR INPUT
    $("#calculator-amount")
        .focus(function() {
            $(this).val("");
            $('#calculator-daily').text('');
            $('#calculator-total').text('');
        })
        .keyup(function() {
            if($(this).val()!=""){
               /* var selectedPlan = $('#calculator-plans .select-list li a.selected').data('minimum');
                var currency_rate = parseInt($('#calculator-currency .select-list li a.selected').data('rate'));
                var planMinimum = selectedPlan.data('minimum')/currency_rate;
*/
                setTimeout(function () {
                    mainCalculator();
                }, 50);
            }
        });
});