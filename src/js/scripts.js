$(document).ready(function(){
    var currentLang = $('html').attr('lang');

    // ========================================================================= REPLACE COMMA FOR CALCULATOR OR DEPOSIT INPUTS
    function replaceComma(e) {
        e.value=e.value.replace(/,/g, '.');
    }

    //=========================================================================== TO TOP
    $(function () {
        var offset = 500,
            offset_opacity = 500,
            scroll_top_duration = 900,
            $back_to_top = $('#top');

        $(window).scroll(function () {
            ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('visible') : $back_to_top.removeClass('visible fade-out');
            if ($(this).scrollTop() > offset_opacity) {
                $back_to_top.addClass('fade-out');
            }
        });

        $back_to_top.on('click', function (event) {
            event.preventDefault();
            $('body,html').animate({
                    scrollTop: 0,
                }, scroll_top_duration
            );
        });

    });

    //=========================================================================== CABINET DEPOSIT PAYMENT SYSTEM
    $('.payment-block').click(function() {
        $('.payment-block').removeClass('active');
        $(this).addClass('active');
        $('.ps-insert').val($(this).attr('data-id'));

        var planID = $(this).data('payment');
        var exchange = 10000;

        if(planID==48) exchange = 10000;
        else if(planID==68) exchange = 200;
        else if(planID==69) exchange = 1000;
        else exchange = 1;
        $('#exchange').val(exchange);

        $('.deposit').removeClass('active');
        $('.deposit.payment-'+planID).addClass('active');

       /* var minimumValue = $('.plan.active').data('minimum');
        var minimumValueExchange = $('.plan.active').data('minimum')/exchange;*/


        var minimumValue = $('#deposit_range_val').data('min');
        var minimumValueExchange = $('#deposit_range_val').data('min')/exchange;


        /*if()*/



        var reinvestBtn = $(this).find('.balance > span').html();
        if(reinvestBtn){
            $('#amount-input-btc').val(reinvestBtn);
            $('#amount-input').val(reinvestBtn*exchange);
        }
        else{
            $('#amount-input-btc').val(minimumValueExchange);
            $('#amount-input').val(minimumValue);
        }

        $('.amount_input_cover').attr('data-payment', planID);

        /*setTimeout(function () {
            calculateDepositPlans();
        }, 50);*/

    });

    // ========================================================================= CABINET DEPOSIT INPUT
    $(function () {
        $('#amount-input-btc').on('input', function () {
            replaceComma(this);

            var exchange = $('#exchange').val();
            var value = $(this).val()*exchange;
            $('#amount-input').val(value);
        });
    });

    // ========================================================================= CABINET DEPOSIT AMOUNT BUTTONS
    $('.amount-block .amount-btn').on('click', function(){
        var selectValueBtc = $(this).data('value');
        var exchange = $('#exchange').val();
        console.log(exchange);
        var selectValue = $(this).data('value')*exchange;
        $('.amount-block .amount-btn').removeClass(('active'));
        $(this).addClass(('active'));

        $('#amount-input-btc').val(selectValueBtc);
        $('#amount-input').val(selectValue);
    });

    //=========================================================================== CABINET WITHDRAW PAYMENT SYSTEM
    $('.withdraw-block').click(function () {
        $('.withdraw-block').removeClass('active');
        $(this).addClass('active');

        var paymentID = $(this).attr('data-payment');
        $('#ec').val(paymentID);

        var exchange = 10000;
        if(paymentID==48) exchange = 10000;
        else if(paymentID==68) exchange = 200;
        else if(paymentID==69) exchange = 1000;
        else exchange = 1;
        $('#exchange').val(exchange);

        var ammounWithdraw = $(this).children(".balance").attr("data-withdraw");
        var ammountWithdrawExchange = ammounWithdraw / exchange;

        $('#amount-btc').val(ammountWithdrawExchange);
        $('#amount').val(ammounWithdraw);

        $('.amount_input_cover').attr('data-payment', paymentID);
    });


    // ========================================================================= CABINET WITHDRAW INPUT
    $(function () {
        $('#amount-btc').on('input', function () {
            replaceComma(this);
            var exchange = $('#exchange').val();
            var value = $(this).val() * exchange;
            $('#amount').val(value);
            /*setTimeout(function () {
                calculateDepositPlans();
            }, 50);*/
        });
    });


    // ========================================================================= BTN COPY REFERRAL
    var copyTextareaBtn = $('.js-textareacopybtn');

    copyTextareaBtn.on('click', function(event) {
        /*if(currentLang == 'ru'){
            $(this).text('Скопировано');
        }
        else {
            $(this).text('Copied');
        }*/
        var copyTextarea = $(this).parent('.referral_item').find('.js-copytextarea');
        copyTextarea.select();
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
        } catch (err) {}

        var that = this;
        /*setTimeout(function () {
            if(currentLang == 'ru'){
                $(that).text('Копировать');
            }
            else {
                $(that).text('Copy');
            }
        }, 2000);*/
    });

    var copyReferralBtn = $('.js-inputcopybtn');

    copyReferralBtn.on('click', function(event) {
        if(currentLang == 'ru'){
            $(this).text('Скопировано');
        }
        else {
            $(this).text('Copied');
        }
        var copyTextarea = $(this).closest('.referral_home_item').find('.js-copyinput');
        copyTextarea.select();
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
        } catch (err) {}

        var that = this;
        setTimeout(function () {
            if(currentLang == 'ru'){
                $(that).text('Копировать');
            }
            else {
                $(that).text('Copy referral link');
            }
        }, 2000);
    });


    var copyReferralBtn = $('.js-inputcopybtn_ref');
    copyReferralBtn.on('click', function(event) {
        var copyTextarea = $(this).closest('.referral_home_item').find('.js-copyinput');
        copyTextarea.select();
        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
        } catch (err) {}

    });


    // ========================================================================= FAQ SECTION
    if($('.faq-tab').length) {
        $(".faq-tab .answer").hide();
        $(document).on('click', '.faq-tab > li > h4', function () {
            if($(this).parent().hasClass('active')){
                $(this).parent().find(".answer").slideUp();
                $(this).parent().removeClass("active");
            }
            else{
                $(this).parent().find(".answer").slideDown();
                $(this).parent().addClass("active");
            }
        });
    };


    // ========================================================================= MENU CABINET
    $(function () {
        $('.user-navigation li a').each(function () {
            var location = window.location.href;
            var link = this.href;
            if(location == link) {
                $(this).addClass('active');
            }
        });
    });

    // ========================================================================= PROMOTIONS CABINET
    $('.btn-banner').on('click', function () {
        $(".btn-banner").removeClass('active');
        $(this).addClass('active');

        var banner_category = $(this).data('banner');
        $(".banner-tab").css('display', 'none');
        $("#" + banner_category).css('display', 'block');
    });


    // ========================================================================= MENU HEADER
    $(function () {
        $('.main-menu li a').each(function () {
            var location = window.location.href;
            var link = this.href;
            if(location == link) {
                $(this).addClass('active');
            }
        });
        $('.popup-top-menu li a').each(function () {
            var location = window.location.href;
            var link = this.href;
            if(location == link) {
                $(this).addClass('active');
            }
        });
    });

    // =========================================================================  popup head-menu
    $('body').on('click', ' .mobileMenu', function() {
        var navMenu = $(this).parent('.mobile');
        if($(navMenu).hasClass('active')){
            $(navMenu).removeClass('active');
        }
        else {
            $(navMenu).addClass('active');
        }
        return false;
    });
    $('body').on('click', '.mobile .popupCloseButton', function() {
        var navMenu = $(this).parent().parent().parent('.mobile');
        if($(navMenu).hasClass('active')){
            $(navMenu).removeClass('active');
        }
        return false;
    });
    $(document).click(function(event) {
        if ( !$(event.target).parent().hasClass('popupContainer')) {
            $('.mobile').removeClass('active');
        }
    });

    // =========================================================================  popup select language
    $(function () {
        $('body').on('click', '.language-href', function() {
            var navMenu = $(this).parent('.select-language');
            if($(navMenu).hasClass('active')){
                $(navMenu).removeClass('active');
            }
            else {
                $(navMenu).addClass('active');
            }
            return false;
        });

    });
    $(document).click(function(event) {
        if ( !$(event.target).hasClass('language-tab')) {
            $('.select-language').removeClass('active');
        }
    });

    // =========================================================================  SIDEBAR TOGGLE
    $(function(){
        if ($(window).width() <= 767) {
            var pageScroll = ($(window).width() > 710);
            $('.user-information > .slimScroll').slimScroll({
                height: 'auto',
                railVisible: false,
                alwaysVisible: true,
                color: '#fab915',
                railColor: '#fab915',
                railOpacity: '1',
                opacity:1,
                allowPageScroll: pageScroll,
                disableFadeOut: true,
                borderRadius:'0px',
                railBorderRadius:'0px',
                position: 'right',
                distance : '0px',
                size: '4px'
            });
        }

        $('body').on('click', '.mobile-user-menu', function() {
            var navMenu = $(this).parent('.user-information');
            if($(navMenu).hasClass('open')){
                $(navMenu).removeClass('open');
            }
            else {
                $(navMenu).addClass('open');
            }
            return false;
        });
    });

    // =========================================================================  HOME PLANS TABS
    $('.plan_tab_link .btn-blue').on('click', function () {
        $(".plan_tab_link .btn-blue").removeClass('active');
        $(this).addClass('active');

        var plan_tab = $(this).data('tab');
        $(".plan_tab").css('display', 'none');
        $("#" + plan_tab).css('display', 'block');
    });


    /* HOME PLANS TAB  */
    $('.deposit_tab_links .deposit_tab_link').on('click', function () {
        $(".deposit_tab_links .deposit_tab_link").removeClass('active');
        $(this).addClass('active');

        var plan_tab = $(this).data('tab');
        $(".depo_plan_tab").css('display', 'none');
        $("#" + plan_tab).css('display', 'block');


        if(plan_tab == 'deptrading'){
            $('#deposit_range_val').val(15);
            $('.pl-insert').val( $('#deposit_range_val').data('id_plan'));
            enterAmout( 15, String(plan_tab));
            window.rangeSlider1 = RangeSlider.create("#deposit_range_deptrading",optionsRange);
            $('.payment-block.active').click();
        }
        if(plan_tab == 'depmining'){
            $('#deposit_range_val').val(20);
            $('.pl-insert').val( $('#deposit_range_val').data('id_plan'));
            enterAmout( 20, String(plan_tab));
            window.rangeSlider2 = RangeSlider.create("#deposit_range_depmining",optionsRange2);
            $('.payment-block.active').click();
        }
        if(plan_tab == 'depinnovation'){
            $('#deposit_range_val').val(90);
            $('.pl-insert').val( $('#deposit_range_val').data('id_plan'));
            enterAmout( 90, String(plan_tab));
            window.rangeSlider3 = RangeSlider.create("#deposit_range_depinnovation",optionsRange3);
            $('.payment-block.active').click();
        }
    });

    // =========================================================================  CABINET HISTORY PAGE
    if($('#invest_type').length){
        $('#invest_type').click(function () {
            var all_tr = $("#investments_table tr.filter_invest_type");
            if($(this).val() == 0){
                $('#investments_table tr.filter_invest_type').css('display' , 'table-row');
            }else {
                for (var i = 0; i < all_tr.length; i++) {
                    if (all_tr[i].dataset.filter == $(this).val()) {
                        all_tr[i].style.display = 'table-row';
                    } else {
                        all_tr[i].style.display = 'none';
                    }
                }
            }
        });
    }
    if($('#dateFrom').length) {
        $("#dateFrom").datepicker({
            dateFormat: "M-d-yy",
            onSelect: function () {
                var currentDate = $("#dateFrom").datepicker("getDate");

                var currDay = currentDate.getDate();
                var currMonth = currentDate.getMonth() + 1;
                var currYear = currentDate.getFullYear();

                document.getElementById("day_from").value = currDay;
                document.getElementById('month_from').value = currMonth;
                document.getElementById('year_from').value = currYear;
            }
        });
    }
    if($('#dateTo').length) {
        $("#dateTo").datepicker({
            dateFormat: "M-d-yy",
            onSelect: function () {
                var currentDate = $("#dateTo").datepicker("getDate");

                var currDay = currentDate.getDate();
                var currMonth = currentDate.getMonth() + 1;
                var currYear = currentDate.getFullYear();

                document.getElementById("day_to").value = currDay;
                document.getElementById('month_to').value = currMonth;
                document.getElementById('year_to').value = currYear;
            }
        });
    }

    // ========================================================================= VIDEO-BLOCK HOMEPAGE
    if($('.video-container').length) {
        var videoContainer = document.querySelector(".video-container"),
            video = videoContainer.querySelector(".video"),
            player = video.querySelector("video"),
            videoCaption = document.querySelector(".video-caption"),
            scrollOffset, SCROLL_COLLAPSE_THRESHOLD = 150, LG_VIEWPORT_WIDTH = 1040, TRANSITION_DURATION = 400,
            hasAutoplayed = !1;
        window.addEventListener("resize", function () {
            updateVideoForViewportSize()
        }), player.addEventListener("play", function () {
            videoContainer.classList.remove("show-play")
        }), player.addEventListener("pause", function () {
            videoContainer.classList.add("show-play")
        });
        var autoplayVideo = function () {
            !hasAutoplayed && video.getBoundingClientRect().top < window.innerHeight - 100 && (hasAutoplayed = !0, player.play())
        }, updateVideoForViewportSize = function () {
            isLargeViewport() ? (isCollapsed() && hideControls(), player.volume = 0) : (video.style.transform = "", collapseVideo(), showControls())
        };
        window.addEventListener("scroll", function () {
            autoplayVideo(), Math.abs(window.scrollY - scrollOffset) > SCROLL_COLLAPSE_THRESHOLD && collapseVideo()
        }), autoplayVideo();
        var setVolume = function (e, t) {
            var n = function (e, t, n, r) {
                return r === 0 ? 0 : n * e / r + t
            }, r, i = player.volume, s = function (o) {
                r = r || o;
                var u = o - r, a = n(u, i, e - i, t);
                player.volume = Math.min(a, 1), u < t ? requestAnimationFrame(s) : player.volume = e
            };
            requestAnimationFrame(s)
        }, showControls = function () {
            player.setAttribute("controls", "true")
        }, hideControls = function () {
            player.removeAttribute("controls")
        }, isLargeViewport = function () {
            return window.innerWidth >= LG_VIEWPORT_WIDTH
        }, isCollapsed = function () {
            return videoContainer.classList.contains("collapsed")
        }, updateFullsizeVideoBounds = function () {
            var e = videoContainer.getBoundingClientRect().top, t = (window.innerHeight - video.offsetHeight) / 2 - e;
            video.style.transform = "translateY(" + t + "px)"
        }, expandVideo = function () {
            isCollapsed() && (setVolume(1, 1500),
                player.play(),
                showControls(),
                video.classList.add("animated"),
                updateFullsizeVideoBounds(),
                scrollOffset = window.scrollY,
                videoContainer.classList.remove("collapsed"),
                //videoCaption.classList.remove("visible"),
                setTimeout(function () {
                    video.classList.remove("animated")
                }, TRANSITION_DURATION), window.siteAnalytics && window.siteAnalytics.trackVideoExpand && window.siteAnalytics.trackVideoExpand(player))
        }, collapseVideo = function () {
            isCollapsed() || (setVolume(0, 0),
                player.pause(),
                hideControls(),
                video.classList.add("animated"),
                videoContainer.classList.add("collapsed"),
                //videoCaption.classList.add("visible"),
                setTimeout(function () {
                    video.classList.remove("animated")
                }, TRANSITION_DURATION))
        };
        document.addEventListener("click", function (e) {
            !isCollapsed() && e.target != video && window.innerWidth >= LG_VIEWPORT_WIDTH && collapseVideo()
        }), video.addEventListener("click", function (e) {
            isLargeViewport() && (e.stopPropagation(), isCollapsed() && expandVideo())
        }), updateVideoForViewportSize();
    }

    // ========================================================================= PARALLAX HOMEPAGE
    //console.log("TOTAL SLIDES: "+countSlidess+"\n");

    var counter=0;
    var countSlidess = $('.js-paralax_zone').length;

    if($('.parallax-slider').length) {
        function parallaxIgniter(count) {
            var prlx = [];
            for (var i = count; i >= 1; i--) {
                var scene = document.getElementById('scene_' + i);
                prlx[i] = new Parallax(scene);
            }
        }
        function parallaxRandomizer(countSlidess, currentNumber) {
            // console.log("CURRENT SLIDE: "+currentNumber+"\n");

            var currentUl = $('.banner_none.js-banner_' + currentNumber);
            if (currentUl.data('bgheader') != undefined) {
                var bgImg = currentUl.data('bgheader');
                if (counter != 0) {
                    $('.parallax-background').animate({
                        opacity: "0"
                    }, 800, "linear", function () {
                        $('.parallax-background').css('background-image', 'url(' + bgImg + ')');
                        $('.parallax-background').animate({
                            opacity: "1"
                        }, 800, "linear", function () {

                        });
                    });
                }
                else {
                    //$('.parallax-background').css('background-image', 'url("img/helloween/background.jpg")');
                }
            }

            var previousNumber;
            if (currentNumber == countSlidess) {
                previousNumber = 1;
            } else {
                previousNumber = currentNumber + 1;
            }
            //console.log("PREVIOUS NUMBER: "+previousNumber+"\n");
            $('.banner_none.js-banner_' + previousNumber).animate({
                opacity: "0"
            }, 800, "linear", function () {
                $('.banner_none.js-banner_' + previousNumber).hide();
                // this reset bar width to 0
                if ($('.js-progress-bar-fill').length) {
                    $('.js-progress-bar-fill').hide().animate({
                        width: '0%'
                    }, 10);
                }
            });
            currentUl.delay(800).show('fast').animate({
                opacity: "1"
            }, 1000, "linear", function () {
                if ($('.js-progress-bar-fill').length) {
                    $('.js-progress-bar-fill').show().animate({
                        width: '100%'
                    }, 8500);
                }
            });
            counter++;
        }
        if ($('.js-paralax_zone ').length) {
            var mainPageParalaxLiLoop;
            if($('.js-paralax_zone ').length == 1){
                // runs when only one slide is available
                var id = $('.js-paralax_zone ').attr('id'),
                    scene = document.getElementById(id),
                    parallax = new Parallax(scene);
                scene.style.display = 'block';
                scene.style.opacity = '1';

            }else if($('.js-paralax_zone ').length > 1){
                // runs when more than one slider is on dom
                var currentNumber = countSlidess;
                parallaxIgniter(countSlidess);
                parallaxRandomizer(countSlidess, currentNumber);

                mainPageParalaxLiLoop = setInterval(function () {
                    currentNumber--;
                    if (currentNumber == 0) {
                        currentNumber = countSlidess;
                    }
                    parallaxRandomizer(countSlidess, currentNumber);
                }, 12000);

                /* $('#parallax-nav a').on('click', function () {
                     clearInterval(mainPageParalaxLiLoop);
                     mainPageParalaxLiLoop = setInterval(function () {
                         randomNumber++;
                         parallaxRandomizer(countSlidess, randomNumber);
                         if (randomNumber == countSlidess) {
                             randomNumber = 0;
                         }
                     }, 12000);
                 });*/

                /*if ($('.js-stop-loop').length) {
                    $('.js-stop-loop').on('click', function () {
                        clearInterval(mainPageParalaxLiLoop);
                    });
                }      */
            }
        }
    }

});